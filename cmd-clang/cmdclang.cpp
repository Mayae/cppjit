// cmdclang.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "libCppJit/libCppJit.h"
#include <string>
#include <filesystem>
#include <crtdbg.h>
#include <iostream>
namespace fs = std::experimental::filesystem;

//int x = [] {return atexit([] { _CrtDumpMemoryLeaks(); }); }();
bool ewait(jit_error_t error, const std::string s)
{
	if (error != jit_error_none)
	{
		printf("operation failed: %s\nwith error: %s\n", s.c_str(), jit_format_error(error));
		system("pause");
		return false;
	}

	return true;

}
#define V(expr) ewait(expr, #expr)

int main(int argc, const char * const * argv)
{
	struct destruct
	{
		jit_context* c;
		~destruct() { jit_delete_context(c); }
	};

	fs::path root = R"(C:\Code\repos\llvm-build\x64\Debug)";

	auto file = (root / "test.cpp").string();
	auto ofile = (root / "test.ll").string();
	const char * args[] = {
		"-cc1", 
		//"-v",
		//"-march=native",
		file.c_str(), 
		"-o", ofile.c_str(),

		//"-S",
		"-emit-llvm"
	};

	if (clang_main("clang", sizeof(args) / sizeof(args[0]), args, argv[0]) == 0)
	{
		jit_context * ctx(jit_create_context());

		destruct d{ ctx };
		void * sym;
		static std::vector<void(*)()> lists;

		auto _atexit = [](void (*v)()) 
		{
			lists.push_back(v);
		};

		jit_set_callback(ctx, nullptr, [](void *, const char * msg, jit_error_t) { std::cout << msg << std::endl; });

		if (!V(jit_add_ir_file(ctx, (root / "test.ll").string().c_str())))
		{
			return -1;
		}
		
		if (!V(jit_inject_symbol(ctx, "printf", printf)))
		{
			return -1;
		}

		/*if (!V(jit_inject_symbol(ctx, "atexit", &_atexit)))
		{
			return -1;
		} */


		if (!V(jit_finalize(ctx)))
		{
			return -1;
		}

		if(!V(jit_get_symbol(ctx, "func", &sym)))
		{
			return -1;
		}

		using fn = int(int);
		((fn*)sym)(3);


		for (auto& l : lists)
		{
			std::cout << "calling an atexit() thing" << std::endl;
			l();
		}
	}
	else
	{
		std::cerr << "Error compiling input" << std::endl;
	}



	//atexit([] { _CrtDumpMemoryLeaks(); });
	system("pause");
    return 0;
}

