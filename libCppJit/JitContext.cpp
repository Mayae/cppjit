#include "libCppJit.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/TargetSelect.h"

#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Support/DynamicLibrary.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.h"
#include "llvm/ExecutionEngine/Orc/IRCompileLayer.h"
#include "llvm/ExecutionEngine/Orc/CompileUtils.h"
#include "llvm/ExecutionEngine/JITSymbol.h"
#include "llvm/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/Orc/LambdaResolver.h"
#include "llvm/ExecutionEngine/SectionMemoryManager.h"

#include <sstream>
#include <string>
#include <memory>
#include <map>

class LLVMGlobalState
{
public:
	LLVMGlobalState()
	{/*
		llvm::PassRegistry &Registry = *llvm::PassRegistry::getPassRegistry();

		initializeTarget(Registry); */

		
		/*
		llvm::PassRegistry &Registry = *llvm::PassRegistry::getPassRegistry();
		initializeCore(Registry);
		initializeScalarOpts(Registry);
		initializeIPO(Registry);
		initializeAnalysis(Registry);
		//initializeIPA(Registry);
		initializeTransformUtils(Registry);
		initializeInstCombine(Registry);
		initializeInstrumentation(Registry);
		initializeTarget(Registry); 
		
		llvm::InitializeAllTargets();
		llvm::InitializeAllTargetMCs();
		llvm::InitializeAllAsmPrinters();
		llvm::InitializeAllAsmParsers(); */
		llvm::sys::DynamicLibrary::LoadLibraryPermanently(nullptr);

	}

	~LLVMGlobalState()
	{

	}
};

struct JitContext
{

	static LLVMGlobalState llState;
	llvm::LLVMContext context;
	llvm::SMDiagnostic diagnostic;

	// jit
	std::unique_ptr<llvm::TargetMachine> targetMachine;
	const llvm::DataLayout layout;
	llvm::orc::RTDyldObjectLinkingLayer objectLayer;
	llvm::orc::IRCompileLayer<decltype(objectLayer), llvm::orc::SimpleCompiler> compileLayer;

	using ModuleHandle = decltype(compileLayer)::ModuleHandleT;
	std::vector<ModuleHandle> jitModules;

	jit_error_callback callback = nullptr;
	void * callbackContext = nullptr;
	bool hasUnresolvedExternals = false;
	std::map<std::string, void *> userSymbols;
	bool isSealed = false;

	bool sealed() const noexcept { return isSealed; }

	void seal() noexcept { isSealed = true; }

	JitContext()
		: targetMachine(llvm::EngineBuilder().selectTarget())
		, layout(targetMachine->createDataLayout())
		, objectLayer(std::make_shared<llvm::SectionMemoryManager>)
		, compileLayer(objectLayer, llvm::orc::SimpleCompiler(*targetMachine))
	{
	}

	void initGlobalRuntime()
	{

	}

	void closeGlobalRuntime()
	{

	}

	void addUserSymbol(std::string s, void * loc)
	{
		userSymbols.emplace(std::move(s), loc);
	}

	void setCallbackContext(jit_error_callback cb, void * context)
	{
		callback = cb;
		callbackContext = context;
	}

	bool addIR(const llvm::StringRef path)
	{
		if (auto m = llvm::parseIRFile(path, diagnostic, context))
		{
			jitModules.emplace_back(addModule(std::move(m)));
			return true;
		}
		else if(callback)
		{
			std::string contents;
			diagnostic.print(path.data(), llvm::raw_string_ostream(contents));

			callback(callbackContext, contents.c_str(), jit_error_parsing);
		}

		return false;
	}

	bool addIRString(const llvm::StringRef contents)
	{
		if (auto m = llvm::parseIR(llvm::MemoryBufferRef(contents, ""), diagnostic, context))
		{
			jitModules.emplace_back(addModule(std::move(m)));
			return true;
		}
		else if (callback)
		{
			std::string response;
			diagnostic.print("<string input>", llvm::raw_string_ostream(response));

			callback(callbackContext, response.c_str(), jit_error_parsing);
		}

		return false;
	}

	jit_error_t getSymbol(const char * name, void ** loc)
	{
		for (auto& m : jitModules)
		{
			if (auto jm = m->get())
			{
				if (auto sm = jm->getSymbol(name, false))
				{
					if (auto ptr = (void *)sm.getAddress().get())
					{
						if (!hasUnresolvedExternals)
						{
							*loc = ptr;
							return jit_error_none;
						}
						else
						{
							return jit_error_unresolved_external;
						}
					}
				}
			}
		}

		return jit_error_not_found;
	}

	ModuleHandle addModule(std::unique_ptr<llvm::Module> M) 
	{
		hasUnresolvedExternals = false;

		auto resolver = llvm::orc::createLambdaResolver(
			[&](const std::string& name)
			{
				auto it = userSymbols.find(name);
				if (it != userSymbols.end())
					return llvm::JITSymbol((std::uint64_t)it->second, llvm::JITSymbolFlags::Exported);

				if (auto sym = compileLayer.findSymbol(name, false))
					return sym;

				return llvm::JITSymbol(nullptr);
			},
			[this](const std::string& name) 
			{
				if (auto SymAddr = llvm::RTDyldMemoryManager::getSymbolAddressInProcess(name))
					return llvm::JITSymbol(SymAddr, llvm::JITSymbolFlags::Exported);

				hasUnresolvedExternals = true;

				if (callback != nullptr)
				{
					std::stringstream s;
					s << "error: unresolved external symbol \"" << name << "\"";
					callback(callbackContext, s.str().c_str(), jit_error_unresolved_external);
				}

				return llvm::JITSymbol(llvm::make_error<llvm::RuntimeDyldError>("Cannot find symbol"));
			}
		);

		// Add the set to the JIT with the resolver we created above and a newly
		// created SectionMemoryManager.
		return llvm::cantFail(compileLayer.addModule(std::move(M),
			std::move(resolver)));
	}



};

LLVMGlobalState JitContext::llState;


JitContext * jit_create_context()
{
	try
	{
		return new JitContext();
	}
	catch (...)
	{
		return nullptr;
	}
}

EXPORT const char * jit_format_error(jit_error_t error)
{
	switch (error)
	{
	case jit_error_none: return "no error";
			case 	jit_error_argument: return "no error";
			case 	jit_error_not_found: return "something wasn't found";
			case 	jit_error_unresolved_external: return "unresolved external symbol";
			case 	jit_error_unfit_stage: return "the operation could not be performed at this stage";
			case 	jit_error_at_previous_stage: return "there was an error at a previous stage";
			case 	jit_error_verbose: return "diagnostic message";
			case 	jit_error_parsing: return "error parsing a file";
	default:
		return "unknown error";
	}
}

jit_error_t jit_open(jit_context * c)
{
	if (!c)
		return jit_error_argument;

	if (!c->sealed())
		return jit_error_unfit_stage;

	c->initGlobalRuntime();

	return jit_error_none;
}

jit_error_t jit_close(jit_context * c)
{
	if (!c)
		return jit_error_argument;

	if (!c->sealed())
		return jit_error_unfit_stage;

	c->closeGlobalRuntime();

	return jit_error_none;
}

jit_error_t jit_finalize(jit_context * c)
{
	if (!c)
		return jit_error_argument;

	if (c->sealed())
		return jit_error_unfit_stage;

	c->seal();

	return jit_error_none;
}

jit_error_t jit_add_ir_file(jit_context * c, const char * file)
{
	if (!c || !file)
		return jit_error_argument;

	if (c->sealed())
		return jit_error_unfit_stage;

	return c->addIR(file) ? jit_error_none : jit_error_parsing;
}

jit_error_t jit_set_callback(jit_context * c, void * callback_context, jit_error_callback cb)
{
	if (!c || !cb)
		return jit_error_argument;

	c->setCallbackContext(cb, callback_context);

	return jit_error_none;
}


jit_error_t jit_get_symbol(JitContext * c, const char * name, void ** loc)
{
	if (!c || !loc || !name)
		return jit_error_argument;

	if (!c->sealed())
		return jit_error_unfit_stage;

	return c->getSymbol(name, loc);
}

jit_error_t jit_inject_symbol(JitContext * c, const char * name, void * loc)
{
	if (!c || !loc || !name)
		return jit_error_argument;

	if (c->sealed())
		return jit_error_unfit_stage;

	c->addUserSymbol(name, loc);

	return jit_error_none;
}

jit_error_t jit_delete_context(JitContext * c)
{
	if (!c)
		return jit_error_argument;

	delete c;
	return jit_error_none;
}
