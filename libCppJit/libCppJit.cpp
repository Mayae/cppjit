//===-- cc1_main.cpp - Clang CC1 Compiler Frontend ------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This is the entry point to the clang -cc1 functionality, which implements the
// core compiler functionality along with a number of additional tools for
// demonstration and testing purposes.
//
//===----------------------------------------------------------------------===//
#include "libCppJit.h"
#include "llvm/Option/Arg.h"
#include "clang/CodeGen/ObjectFilePCHContainerOperations.h"
#include "clang/Config/config.h"
#include "clang/Driver/ToolChain.h"
#include "clang/Driver/DriverDiagnostic.h"
#include "clang/Driver/Options.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/FrontendDiagnostic.h"
#include "clang/Frontend/TextDiagnosticBuffer.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Frontend/Utils.h"
#include "clang/FrontendTool/Utils.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/LinkAllPasses.h"
#include "llvm/Option/ArgList.h"
#include "llvm/Option/OptTable.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/Signals.h"
#include "llvm/Support/StringSaver.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/Timer.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Process.h"
#include "llvm/Support/Program.h"
#include "llvm/Support/Path.h"
#include <cstdio>
#include "clang/Driver/ToolChain.h"


using namespace clang;
using namespace clang::driver;
using namespace llvm::opt;

std::string GetExecutablePath(const char *Argv0, bool CanonicalPrefixes)
{
	if (!CanonicalPrefixes) {
		SmallString<128> ExecutablePath(Argv0);
		// Do a PATH lookup if Argv0 isn't a valid path.
		if (!llvm::sys::fs::exists(ExecutablePath))
			if (llvm::ErrorOr<std::string> P =
				llvm::sys::findProgramByName(ExecutablePath))
				ExecutablePath = *P;
		return ExecutablePath.str();
	}

	// This just needs to be some symbol in the binary; C++ doesn't
	// allow taking the address of ::main however.
	void *P = (void*)(intptr_t)GetExecutablePath;
	return llvm::sys::fs::getMainExecutable(Argv0, P);
}

//===----------------------------------------------------------------------===//
// Main driver
//===----------------------------------------------------------------------===//

static void LLVMErrorHandler(void *UserData, const std::string &Message,
	bool GenCrashDiag) {
	DiagnosticsEngine &Diags = *static_cast<DiagnosticsEngine*>(UserData);

	Diags.Report(diag::err_fe_error_backend) << Message;

	// Run the interrupt handlers to make sure any special cleanups get done, in
	// particular that we remove files registered with RemoveFileOnSignal.
	llvm::sys::RunInterruptHandlers();

	// We cannot recover from llvm errors.  When reporting a fatal error, exit
	// with status 70 to generate crash diagnostics.  For BSD systems this is
	// defined as an internal software error.  Otherwise, exit with status 1.
	exit(GenCrashDiag ? 70 : 1);
}

int cc1_internal(ArrayRef<const char *> Argv, const char *Argv0, void *MainAddr) 
{

	std::unique_ptr<CompilerInstance> Clang(new CompilerInstance());
	IntrusiveRefCntPtr<DiagnosticIDs> DiagID(new DiagnosticIDs());

	// Register the support for object-file-wrapped Clang modules.
	auto PCHOps = Clang->getPCHContainerOperations();
	PCHOps->registerWriter(llvm::make_unique<ObjectFilePCHContainerWriter>());
	PCHOps->registerReader(llvm::make_unique<ObjectFilePCHContainerReader>());

	// Initialize targets first, so that --version shows registered targets.
	llvm::InitializeAllTargets();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmPrinters();
	llvm::InitializeAllAsmParsers();

#ifdef LINK_POLLY_INTO_TOOLS
	llvm::PassRegistry &Registry = *llvm::PassRegistry::getPassRegistry();
	polly::initializePollyPasses(Registry);
#endif

	// Buffer diagnostics from argument parsing so that we can output them using a
	// well formed diagnostic object.
	IntrusiveRefCntPtr<DiagnosticOptions> DiagOpts = new DiagnosticOptions();
	TextDiagnosticBuffer *DiagsBuffer = new TextDiagnosticBuffer;
	DiagnosticsEngine Diags(DiagID, &*DiagOpts, DiagsBuffer);
	bool Success = CompilerInvocation::CreateFromArgs(
		Clang->getInvocation(), Argv.begin(), Argv.end(), Diags);

	// Infer the builtin include path if unspecified.
	if (Clang->getHeaderSearchOpts().UseBuiltinIncludes &&
		Clang->getHeaderSearchOpts().ResourceDir.empty())
		Clang->getHeaderSearchOpts().ResourceDir =
		CompilerInvocation::GetResourcesPath(Argv0, MainAddr);

	// Create the actual diagnostics engine.
	Clang->createDiagnostics();
	if (!Clang->hasDiagnostics())
		return 1;

	// Set an error handler, so that any LLVM backend diagnostics go through our
	// error handler.
	llvm::install_fatal_error_handler(LLVMErrorHandler,
		static_cast<void*>(&Clang->getDiagnostics()));

	DiagsBuffer->FlushDiagnostics(Clang->getDiagnostics());
	if (!Success)
		return 1;

	// Execute the frontend actions.
	Success = ExecuteCompilerInvocation(Clang.get());

	// If any timers were active but haven't been destroyed yet, print their
	// results now.  This happens in -disable-free mode.
	llvm::TimerGroup::printAll(llvm::errs());

	// Our error handler depends on the Diagnostics object, which we're
	// potentially about to delete. Uninstall the handler now so that any
	// later errors use the default handling behavior instead.
	llvm::remove_fatal_error_handler();

	// When running with -disable-free, don't do any destruction or shutdown.
	if (Clang->getFrontendOpts().DisableFree) {
		BuryPointer(std::move(Clang));
		return !Success;
	}

	return !Success;
}


int clang_main(const char * toolName, int argc_, const char * const * argv_, const char * argv0)
{
	llvm::PrettyStackTraceProgram X(argc_, argv_);
	//llvm::llvm_shutdown_obj Y; // Call llvm_shutdown() on exit.

	SmallVector<const char *, 256> argv(argv_, argv_ + argc_);

	llvm::InitializeAllTargets();
	auto TargetAndMode = ToolChain::getTargetAndModeFromProgramName(argv[0]);


	llvm::BumpPtrAllocator A;
	llvm::StringSaver Saver(A);

	// Parse response files using the GNU syntax, unless we're in CL mode. There
	// are two ways to put clang in CL compatibility mode: argv[0] is either
	// clang-cl or cl, or --driver-mode=cl is on the command line. The normal
	// command line parsing can't happen until after response file parsing, so we
	// have to manually search for a --driver-mode=cl argument the hard way.
	// Finally, our -cc1 tools don't care which tokenization mode we use because
	// response files written by clang will tokenize the same way in either mode.
	bool ClangCLMode = false;
	if (StringRef(TargetAndMode.DriverMode).equals("--driver-mode=cl") ||
		std::find_if(argv.begin(), argv.end(), [](const char *F) {
		return F && strcmp(F, "--driver-mode=cl") == 0;
	}) != argv.end()) {
		ClangCLMode = true;
	}
	enum { Default, POSIX, Windows } RSPQuoting = Default;
	for (const char *F : argv) {
		if (strcmp(F, "--rsp-quoting=posix") == 0)
			RSPQuoting = POSIX;
		else if (strcmp(F, "--rsp-quoting=windows") == 0)
			RSPQuoting = Windows;
	}

	// Determines whether we want nullptr markers in argv to indicate response
	// files end-of-lines. We only use this for the /LINK driver argument with
	// clang-cl.exe on Windows.
	bool MarkEOLs = ClangCLMode;

	llvm::cl::TokenizerCallback Tokenizer;
	if (RSPQuoting == Windows || (RSPQuoting == Default && ClangCLMode))
		Tokenizer = &llvm::cl::TokenizeWindowsCommandLine;
	else
		Tokenizer = &llvm::cl::TokenizeGNUCommandLine;

	if (MarkEOLs && argv.size() > 1 && StringRef(argv[1]).startswith("-cc1"))
		MarkEOLs = false;
	llvm::cl::ExpandResponseFiles(Saver, Tokenizer, argv, MarkEOLs);

	// Handle -cc1 integrated tools, even if -cc1 was expanded from a response
	// file.
	auto FirstArg = std::find_if(argv.begin(), argv.end(),
		[](const char *A) { return A != nullptr; });
	if (FirstArg != argv.end()) 
	{
		// If -cc1 came from a response file, remove the EOL sentinels.
		if (MarkEOLs) 
		{
			auto newEnd = std::remove(argv.begin(), argv.end(), nullptr);
			argv.resize(newEnd - argv.begin());
		}
		return cc1_internal(argv, argv0, GetExecutablePath);
	}

	return -1;
}