#ifndef LIBCPPJIT_H
#define LIBCPPJIT_H
#ifdef libCppJit_EXPORTS
#define EXPORT extern "C" __declspec(dllexport) 
#else
#define EXPORT extern "C" __declspec(dllimport) 
#endif

EXPORT int clang_main(const char* toolName, int argc, const char * const * argv, const char * argv0);

typedef struct JitContext jit_context;

typedef enum
{
	jit_error_none,
	jit_error_argument,
	jit_error_not_found,
	jit_error_unresolved_external, 
	jit_error_unfit_stage,
	jit_error_at_previous_stage,
	jit_error_verbose,
	jit_error_parsing

} jit_error_t;

typedef void(*jit_error_callback)(void * ctx, const char * message, jit_error_t error_type);


EXPORT const char * jit_format_error(jit_error_t error);
EXPORT jit_context * jit_create_context();
EXPORT jit_error_t jit_add_ir_file(jit_context * c, const char * path);
EXPORT jit_error_t jit_add_ir(jit_context * c, const char * contents);
EXPORT jit_error_t jit_set_callback(jit_context * c, void * callback_context, jit_error_callback cb);
EXPORT jit_error_t jit_delete_context(jit_context * c);
EXPORT jit_error_t jit_get_symbol(jit_context * c, const char * name, void ** ret);
EXPORT jit_error_t jit_inject_symbol(jit_context * c, const char * name, void * location);
EXPORT jit_error_t jit_finalize(jit_context * c);
EXPORT jit_error_t jit_open(jit_context * c);
EXPORT jit_error_t jit_close(jit_context * c);

#endif